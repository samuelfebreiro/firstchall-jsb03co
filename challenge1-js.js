console.log("---calculadora, FUNCIONA---");

function calculator(num1, num2) {
  const type = prompt(
    "Choose sum, subtraction, multiplication, division or exponentiation"
  );

  if (type === "sum") {
    return num1 + num2;
  } else if (type === "subtraction") {
    return num1 - num2;
  } else if (type === "multiplication") {
    return num1 * num2;
  } else if (type === "division") {
    return num1 / num2;
  } else if (type === "exponentiation") {
    return num1 ** num2;
  } else return "Select a correct option please";
}

console.log(calculator(2, 3));

console.log("---academic record, NO FUNCIONA---");

class Person {
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }
  viewPropertiesOfPerson(name, age, gender) {
    return (
      "Name: " +
      this.name +
      " " +
      "Age: " +
      this.age +
      " " +
      "Gender: " +
      this.gender
    );
  }
}

class Teacher extends Person {
  constructor(name, age, gender, subjects, studentsList) {
    super(name, age, gender);
    this.subjects = subjects;
    this.studentsList = [];
  }

  pupilsToProffesor(name, age, gender, subjects) {}
}

class Student extends Person {
  constructor(name, age, gender, course, group) {
    super(name, age, gender);
    this.course = course;
    this.group = group;
  }

  registerNewStudent() {}
}

//showMeTheTeachers()

let myPerson = new Person("Alex", 10, "Male");
console.log(myPerson.viewPropertiesOfPerson());

const samuel = new Student("Samuel", 30, "Male", "1er curso", "2º grupo");
const mateo = new Student("Mateo", 20, "Male", "1er curso", "2º grupo");
const berto = new Teacher("Berto", 30, "Male", "JS avanzado");
const ivan = new Teacher("Ivan", 30, "Male", "JavaScript");
const david = new Teacher("David", 30, "Male", "Tutorias");

console.log("---electronic dice, FUNCIONA---");
function electronicDice() {
  let gameOver = 0;
  let dice = Math.floor(Math.random() * 7);
  while (gameOver < 50) {
    console.log("Number: " + (dice = Math.floor(Math.random() * 7)));

    gameOver += dice;
  }

  return "Ya te llega, " + "van " + gameOver;
}

console.log(electronicDice());
console.log("End of the game");
